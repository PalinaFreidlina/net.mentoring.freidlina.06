﻿using System.Collections.Generic;

namespace DataModel
{
    public class Book: LibraryItem
    {
        public IEnumerable<Person> Authors { get; set; }
        public string PublicationCity { get; set; }
        public string Publisher { get; set; }

        public Book() { }
        public Book(LibraryItem item): base(item) { } 
    }
}
