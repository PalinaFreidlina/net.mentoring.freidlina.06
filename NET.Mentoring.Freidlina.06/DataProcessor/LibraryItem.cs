﻿using System;

namespace DataModel
{
    public class LibraryItem: IItem
    {
        public string Title { get; set; }
        public string Identifier { get; set; }
        public DateTime PublicationDate { get; set; }
        public int SheetsCount { get; set; }
        public string Notes { get; set; }

        public LibraryItem() { }

        public LibraryItem(LibraryItem item)
        {
            Title = item.Title;
            Identifier = item.Identifier;
            PublicationDate = item.PublicationDate;
            SheetsCount = item.SheetsCount;
            Notes = item.Notes;
        }
    }
}
