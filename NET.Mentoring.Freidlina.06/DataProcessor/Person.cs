﻿namespace DataModel
{
    public class Person
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }

        public Person() { }

        public Person(Person person)
        {
            FirstName = person.FirstName;
            SecondName = person.SecondName;
        }
    }
}
