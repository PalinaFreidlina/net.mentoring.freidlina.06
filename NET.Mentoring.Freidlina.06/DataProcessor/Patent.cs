﻿using System;
using System.Collections.Generic;

namespace DataModel
{
    public class Patent: LibraryItem
    {
        public IEnumerable<Person> Authors { get; set; }
        public string Country { get; set; }
        public DateTime RegistrationDate { get; set; }

        public Patent() { }
        public Patent(LibraryItem item): base(item) { }

    }
}
