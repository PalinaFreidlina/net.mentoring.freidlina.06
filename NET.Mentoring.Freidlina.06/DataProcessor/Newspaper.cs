﻿namespace DataModel
{
    public class Newspaper: LibraryItem
    {
        public string PublicationCity { get; set; }
        public string Publisher { get; set; }
        public int Number { get; set; }

        public Newspaper() { }
        public Newspaper(LibraryItem item): base(item) { }
    }
}
