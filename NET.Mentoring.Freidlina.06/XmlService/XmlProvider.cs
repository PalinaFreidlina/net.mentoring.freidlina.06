﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DataModel;

namespace XmlService
{
    public class XmlProvider: IXmlProvider
    {
        private readonly XmlReader _reader = null;

        public XmlProvider(string xmlFileName)
        {
            _reader = XmlReader.Create(xmlFileName);
        }

        public IEnumerable<LibraryItem> GetItems()
        {
            using (_reader)
            {
                while (_reader.Read())
                {
                    if (_reader.NodeType == XmlNodeType.Element)
                        switch (_reader.Name)
                        {
                            case "book":
                                yield return GetBookItem();
                                break;
                            case "newspaper":
                                yield return GetNewspaperItem();
                                break;
                            case "patent":
                                yield return GetPatentItem();
                                break;
                        }
                    if (IsEndOfElement("libraryItems"))
                        yield break;
                }
            }
        }
        
        private Book GetBookItem()
        {
            Book book = new Book(GetBasicLibraryItemInfo());
            string element = "";
            while (_reader.Read())
            {
                if (_reader.NodeType == XmlNodeType.Element)
                {
                    if (_reader.Name == "authors")
                    {
                        book.Authors = GetAuthors().ToList();
                    }
                    else
                    {
                        element = _reader.Name;
                        continue;
                    }
                }
                if (_reader.NodeType == XmlNodeType.Text)
                    switch (element)
                    {
                        case "publicationCity":
                            book.PublicationCity = _reader.Value;
                            break;
                        case "publisher":
                            book.Publisher = _reader.Value;
                            break;
                    }
                if (IsEndOfElement("book"))
                    return book;
            }
            return book;
        }

        private LibraryItem GetBasicLibraryItemInfo()
        {
            LibraryItem item = new LibraryItem();
            string element = "";
            while (_reader.Read())
            {
                if (_reader.NodeType == XmlNodeType.Element)
                {
                    element = _reader.Name;
                    continue;
                }
                if (_reader.NodeType == XmlNodeType.Text)
                    switch (element)
                    {
                        case "title":
                            item.Title = _reader.Value;
                            break;
                        case "identifier":
                            item.Identifier = _reader.Value;
                            break;
                        case "publicationDate":
                            item.PublicationDate = Convert.ToDateTime(_reader.Value);
                            break;
                        case "sheetsCount":
                            item.SheetsCount = Convert.ToInt32(_reader.Value);
                            break;
                        case "notes":
                            item.Notes = _reader.Value;
                            break;
                    }
                if (IsEndOfElement("notes"))
                    return item;
            }
            return item;
        }

        private IEnumerable<Person> GetAuthors()
        {
            Person author = new Person();
            string element = "";
            while (_reader.Read())
            {
                if (_reader.NodeType == XmlNodeType.Element)
                {
                    element = _reader.Name;
                    continue;
                }
                if (_reader.NodeType == XmlNodeType.Text)
                    switch (element)
                    {
                        case "firstName": author.FirstName = _reader.Value;
                            break;
                        case "secondName": author.SecondName = _reader.Value;
                            break;
                    }
                if (_reader.NodeType == XmlNodeType.EndElement)
                    switch (_reader.Name)
                    {
                        case "author":
                            yield return new Person(author);
                            break;
                        case "authors":
                            yield break;
                    }
            }
        }
        private Newspaper GetNewspaperItem()
        {
            Newspaper newspaper = new Newspaper(GetBasicLibraryItemInfo());
            string element = "";
            while (_reader.Read())
            {
                if (_reader.NodeType == XmlNodeType.Element)
                {
                    element = _reader.Name;
                    continue;
                }
                if (_reader.NodeType == XmlNodeType.Text)
                    switch (element)
                    {
                        case "number":
                            newspaper.Number = Convert.ToInt32(_reader.Value);
                            break;
                        case "publicationCity":
                            newspaper.PublicationCity = _reader.Value;
                            break;
                        case "publisher":
                            newspaper.Publisher = _reader.Value;
                            break;
                    }
                if (IsEndOfElement("newspaper"))
                    return newspaper;
            }
            return newspaper;
        }
        private Patent GetPatentItem()
        {
            Patent patent = new Patent(GetBasicLibraryItemInfo());
            string element = "";
            while (_reader.Read())
            {
                if (_reader.NodeType == XmlNodeType.Element)
                {
                    if (_reader.Name == "authors")
                    {
                        patent.Authors = GetAuthors().ToList();
                    }
                    else
                    {
                        element = _reader.Name;
                        continue;
                    }
                }
                if (_reader.NodeType == XmlNodeType.Text)
                    switch (element)
                    {
                        case "country":
                            patent.Country = _reader.Value;
                            break;
                        case "registrationDate":
                            patent.RegistrationDate = Convert.ToDateTime(_reader.Value);
                            break;
                    }
                if (IsEndOfElement("patent"))
                    return patent;
            }
            return patent;
        }

        private bool IsEndOfElement(string element) => _reader.NodeType == XmlNodeType.EndElement && _reader.Name == element;
    }
}
