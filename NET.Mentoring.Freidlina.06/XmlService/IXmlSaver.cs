﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel;

namespace XmlService
{
    public interface IXmlSaver
    {
        void SaveLibraryItems(IEnumerable<LibraryItem> items);
        void SaveLibraryItems(params LibraryItem[] items);
        void SaveItem(LibraryItem item);
    }
}
