﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using DataModel;

namespace XmlService
{
    public class XmlSaver: IXmlSaver
    {
        private XmlWriter writer = null;

        public XmlSaver(string outputFileName)
        {
            XmlWriterSettings ws = new XmlWriterSettings {Indent = true};
            writer = XmlWriter.Create(outputFileName, ws);
        }

        public void SaveLibraryItems(IEnumerable<LibraryItem> items)
        {
            using (writer)
            {
                writer.WriteStartElement("libraryItems");
                foreach (var libraryItem in items)
                {
                    WriteItem(libraryItem);
                }
                writer.WriteEndElement();
            }
        }

        public void SaveLibraryItems(params LibraryItem[] items)
        {
            SaveLibraryItems(items.AsEnumerable());
        }

        public void SaveItem(LibraryItem item)
        {
            using (writer)
            {
                writer.WriteStartElement("libraryItems");
                WriteItem(item);
                writer.WriteEndElement();
            }
        }

        private void WriteItem(LibraryItem item)
        {
            switch (item)
            {
                case Book _:
                    writer.WriteStartElement("book");
                    WriteBook((Book)item);
                    break;
                case Newspaper _:
                    writer.WriteStartElement("newspaper");
                    WriteNewspaper((Newspaper)item);
                    break;
                case Patent _:
                    writer.WriteStartElement("patent");
                    WritePatent((Patent)item);
                    break;
            }
            writer.WriteEndElement();
        }

        private void WriteBook(Book book)
        {
            WriteLibraryItem(book);
            WriteAuthors(book.Authors);
            WriteElement("publicationCity",book.PublicationCity);
            WriteElement(nameof(book.Publisher).ToLower(),book.Publisher);
        }

        private void WriteNewspaper(Newspaper newspaper)
        {
            WriteLibraryItem(newspaper);
            WriteElement("publicationCity", newspaper.PublicationCity);
            WriteElement(nameof(newspaper.Publisher).ToLower(), newspaper.Publisher);
            WriteElement(nameof(newspaper.Number).ToLower(), newspaper.Number.ToString());
        }

        private void WritePatent(Patent patent)
        {
            WriteLibraryItem(patent);
            WriteAuthors(patent.Authors);
            WriteElement(nameof(patent.Country).ToLower(),patent.Country);
            WriteElement("registrationDate",patent.RegistrationDate.ToString(CultureInfo.InvariantCulture));
        }

        private void WriteAuthors(IEnumerable<Person> authors)
        {
            writer.WriteStartElement("authors");
            foreach (var author in authors)
            {
                writer.WriteStartElement("author");
                WriteElement("firstName", author.FirstName);
                WriteElement("secondName", author.SecondName);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        private void WriteLibraryItem(LibraryItem item)
        {
            WriteElement(nameof(item.Title).ToLower(), item.Title);
            WriteElement(nameof(item.Identifier).ToLower(),item.Identifier);
            WriteElement("publicationDate",item.PublicationDate.ToString(CultureInfo.InvariantCulture));
            WriteElement("sheetsCount",item.SheetsCount.ToString());
            WriteElement(nameof(item.Notes).ToLower(), item.Notes);
        }

        private void WriteElement(string name, string text)
        {
            writer.WriteStartElement(name);
            writer.WriteString(text);
            writer.WriteEndElement();
        }
    }
}
