﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;
using XmlService;

namespace TestXmlService
{
    [TestFixture]
    public class XmlManagerTests
    {
        private readonly string testXmlFilePath = @"D:\Learn\Mentoring\Tasks\NET.Mentoring.Freidlina.06\NET.Mentoring.Freidlina.06\TestXmlService\Library.xml";
        public static IEnumerable<TestCaseData> TestCasesForGetItems
        {
            get
            {
                yield return new TestCaseData().Returns(6);
            }
        }
        [Test, TestCaseSource(nameof(TestCasesForGetItems))]
        public int TestGetItems()
        {
            var xml = new XmlProvider(testXmlFilePath);
            var res = xml.GetItems().ToList();
            return res.Count;
        }

        [Test]
        public void TestSaveItem()
        {
            var saver = new XmlSaver(@"D:\Learn\Mentoring\Tasks\NET.Mentoring.Freidlina.06\NET.Mentoring.Freidlina.06\TestXmlService\test.xml");
            var items = new XmlProvider(testXmlFilePath);
            saver.SaveItem(items.GetItems().FirstOrDefault());
        }

        [Test]
        public void TestSaveItems()
        {
            var saver = new XmlSaver(@"D:\Learn\Mentoring\Tasks\NET.Mentoring.Freidlina.06\NET.Mentoring.Freidlina.06\TestXmlService\test.xml");
            var items = new XmlProvider(testXmlFilePath);
            saver.SaveLibraryItems(items.GetItems());
        }
    }
}
